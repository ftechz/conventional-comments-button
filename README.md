# Conventional Comments button

This is a fork of the original [Conventional Comments button](https://gitlab.com/conventionalcomments/conventional-comments-button) project with the ability to enable on multiple hosts via user configuration.

It is also published on the Chrome Store!

> This is a tiny extension that adds a conventional comment button to GitLab file explorer comments, allowing to quickly leave a structured semantic comment during your MR reviews!

## How to install

Visit the extension page in the Chrome store and click the Add to Chrome extension. This also works on Edge and other Chrome based browsers.

<https://chrome.google.com/webstore/detail/conventional-comments-but/pimlnmoahkppoahhfljddkaoefbkdikf>

Unfortunately, Firefox is not supported with this fork due to lack of support of Manifest v3. It may work in the future once Firefox implements support for this. See <https://blog.mozilla.org/addons/2022/05/18/manifest-v3-in-firefox-recap-next-steps/> 

## How to enable it on a self-hosted instance

- Open the extension options
- Add your domain as a new line in the "Enabled Hosts" field
- Click Save

## Demo

![Demo](demo.gif)

## Development

First, clone this repo `git clone git@gitlab.com:conventionalcomments/conventional-comments-button.git` and then see below for browser specific instructions.

Recently chrome disallowed to install packed `crx` extension that are not listed on the Chrome Store, so to install this

- On Chrome: Menu
  - More Tools
    - Extensions (be sure to have _Developer Mode_ enabled there)
- In the Extension page: `Load unpacked` and select the cloned repository

### How to update

- `git pull`

#### Chrome

- On Chrome: Menu
  - More Tools
    - Extensions
- In the Extension page find `conventional comments button` and hit the refresh button

#### Firefox

- On Firefox: enter `about:debugging#/runtime/this-firefox` into the address bar
- In the Extension page find `conventional comments button` and hit the reload button

## Credits

This project bundles some of the icon coming from [font-awesome](https://fontawesome.com/) icons as SVG
